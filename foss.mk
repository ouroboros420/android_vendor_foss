# Apps
PRODUCT_PACKAGES += \
	AdAway \
	AuroraStore \
	Bromite \
	BromiteSystemWebview \
	OrganicMaps

# microG
PRODUCT_PACKAGES += \
    GmsCore \
    GsfProxy \
    FakeStore \
    DejaVuLocationService \
    MozillaNlpBackend \
    NominatimNlpBackend
